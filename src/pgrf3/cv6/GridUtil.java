package pgrf3.cv6;

public class GridUtil {
    public static float[] createVB(int m, int n){
        if (m<1 || n<1)
            return null;
        float[] vb = new float[(m+1)*(n+1)*2*2];
        int indx=0;
        for(int i= 0; i<=n; i++)
            for(int j = 0; j<=m; j++){
                vb[indx++] = j*1f/(m);
                vb[indx++] = i*1f/(n);
            }
        for(float f:vb){
            System.out.print(f +", ");
        }
        System.out.println();

        return vb;

    }

    public static int[] createIB(int m, int n){
        /*float[] vertexBuffer = new float[2 * m * n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                vertexBuffer[(i * n + j) * 2] = (float) i / (m - 1);
                vertexBuffer[(i * n + j) * 2 + 1] = (float) j / (n - 1);
                // System.out.println(vertexBuffer[i*m+j] + " " +
                // vertexBuffer[i*m+j+1]);
            }
        }*/
        if (m<1 || n<1)
            return null;

        int[] indexBuffer = new int[6 * (m ) * (n )];
        int z = 0;
        for (int j = 0; j < n ; j++)
            for (int k = 0; k < m ; k++) {
                int i = j * (m+1) + k;

                indexBuffer[z] = i;
                indexBuffer[z + 1] = i + m + 2;
                indexBuffer[z + 2] = i + m + 1;
                indexBuffer[z + 3] = i + 1;
                indexBuffer[z + 4] = i + m + 2;
                indexBuffer[z + 5] = i;
                z += 6;
            }
        for(int i:indexBuffer){
            System.out.print(i +", ");
        }
        System.out.println();

        return indexBuffer;

    }
}
