package pgrf3.cv5;


import lvl2advanced.p01gui.p01simple.AbstractRenderer;
import lwjglutils.*;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.*;
import transforms.*;

import java.io.IOException;
import java.nio.DoubleBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;


/**
 *
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2019-09-02
 */
public class Renderer extends AbstractRenderer{

	boolean mouseButton1 = false,fill = true, persp = true, depthTest = true;

	int locType,locMatModelL,locMatModelV,locMatViewL,locMatViewV,locMatProjL,locMatProjV, locTime, locPerVertex, locAnim, locLight, locFunc,shaderProgramLight,shaderProgramView;
	int width, height,perVertex,anim = 2;
	Vec3D light_position = new Vec3D(10,10,10);
	double ox, oy;
	float time = 0,types = 1, func = 0, xrot = 0, yrot = 0,zrot = 0, xlight = 0;

	OGLBuffers buffers, buffers2;
	OGLTexture2D texture;
	OGLTexture2D.Viewer textureViewer;
	OGLTextRenderer textRenderer;
	OGLRenderTarget renderTarget;


	Camera cam = new Camera();
	Mat4 proj = new Mat4PerspRH(Math.PI / 4, 1, 0.01, 1000.0);
	Mat4 model = new Mat4Identity();
	// The window handle
	GridUtil g = new GridUtil();



	private GLFWKeyCallback   keyCallback = new GLFWKeyCallback() {
		@Override
		public void invoke(long window, int key, int scancode, int action, int mods) {
			if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
				glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
			if (action == GLFW_PRESS || action == GLFW_REPEAT){
				switch (key) {
					case GLFW_KEY_W:
						cam = cam.forward(1);
						break;
					case GLFW_KEY_D:
						cam = cam.right(1);
						break;
					case GLFW_KEY_S:
						cam = cam.backward(1);
						break;
					case GLFW_KEY_A:
						cam = cam.left(1);
						break;
					case GLFW_KEY_1:
						xrot += 0.1;
						break;
					case GLFW_KEY_2:
						xrot -= 0.1;
						break;
					case GLFW_KEY_3:
						yrot += 0.1;
						//light[1] += light[1] + 1;
						break;
					case GLFW_KEY_4:
						//light[1] += light[1] - 1;
						yrot -= 0.1;
						break;
					case GLFW_KEY_5:
						//light[1] += light[1] - 1;
						zrot += 0.1;
						break;
					case GLFW_KEY_6:
						//light[1] += light[1] - 1;
						zrot -= 0.1;
						break;
					case GLFW_KEY_UP:
						light_position.mul(2);
						break;
					case GLFW_KEY_DOWN:
						light_position.mul(-2);
						break;
					case GLFW_KEY_LEFT_CONTROL:
						cam = cam.down(1);
						break;
					case GLFW_KEY_LEFT_SHIFT:
						cam = cam.up(1);
						break;
					case GLFW_KEY_SPACE:
						cam = cam.withFirstPerson(!cam.getFirstPerson());
						break;
					case GLFW_KEY_P:
						persp = !persp;
						break;
					case GLFW_KEY_V:
						depthTest = !depthTest;
						break;
					case GLFW_KEY_T:
						cam = cam.mulRadius(0.5f);
						break;
					case GLFW_KEY_G:
						cam = cam.mulRadius(1.5f);
						break;
					case GLFW_KEY_F:
						fill = !fill;
						break;
					case GLFW_KEY_H:
						func = (func + 1) %  7;
						break;
					case GLFW_KEY_C:
						anim = (anim + 1) %  3;
						break;
					case GLFW_KEY_R:
						types = (types + 1) %  6;
						System.out.println(types);
						break;
					case GLFW_KEY_Q:
						perVertex = (perVertex + 1) %  2;
						break;
				}
			}
		}
	};

	private GLFWWindowSizeCallback wsCallback = new GLFWWindowSizeCallback() {
		@Override
		public void invoke(long window, int w, int h) {
			if (w > 0 && h > 0 &&
					(w != width || h != height)) {
				width = w;
				height = h;
				proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
				if (textRenderer != null)
					textRenderer.resize(width, height);
			}
		}
	};

	private GLFWMouseButtonCallback mbCallback = new GLFWMouseButtonCallback () {
		@Override
		public void invoke(long window, int button, int action, int mods) {
			mouseButton1 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;

			if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS){
				mouseButton1 = true;
				DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
				DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
				glfwGetCursorPos(window, xBuffer, yBuffer);
				ox = xBuffer.get(0);
				oy = yBuffer.get(0);
			}
			if (button==GLFW_MOUSE_BUTTON_2 && action == GLFW_PRESS){
				//mouseButton1 = true;
				DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
				DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
				glfwGetCursorPos(window, xBuffer, yBuffer);
				//xlight += (float) xBuffer.get(0);
				//light_position.(new Vec3D(xBuffer.get(0),yBuffer.get(0),0));
			}


			if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE){
				mouseButton1 = false;
				DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
				DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
				glfwGetCursorPos(window, xBuffer, yBuffer);
				double x = xBuffer.get(0);
				double y = yBuffer.get(0);
				cam = cam.addAzimuth(Math.PI * (ox - x) / width)
						.addZenith(Math.PI * (oy - y) / width);
				ox = x;
				oy = y;
			}
		}
	};

	private GLFWCursorPosCallback cpCallbacknew = new GLFWCursorPosCallback() {
		@Override
		public void invoke(long window, double x, double y) {
			if (mouseButton1) {
				cam = cam.addAzimuth(Math.PI * (ox - x) / width)
						.addZenith(Math.PI * (oy - y) / width);
				ox = x;
				oy = y;
			}
		}
	};

	private GLFWScrollCallback scrollCallback = new GLFWScrollCallback() {
		@Override
		public void invoke(long window, double dx, double dy) {
			if (dy < 0)
				cam = cam.mulRadius(0.5f);
			else
				cam = cam.mulRadius(1.5f);
		}
	};

	@Override
	public GLFWKeyCallback getKeyCallback() {
		return keyCallback;
	}

	@Override
	public GLFWWindowSizeCallback getWsCallback() {
		return wsCallback;
	}

	@Override
	public GLFWMouseButtonCallback getMouseCallback() {
		return mbCallback;
	}

	@Override
	public GLFWCursorPosCallback getCursorCallback() {
		return cpCallbacknew;
	}

	@Override
	public GLFWScrollCallback getScrollCallback() {
		return scrollCallback;
	}


	void createBuffers(int m) {
		float[] vertexBufferData = g.createVB(m,m);
		int[] indexBufferData = g.createIB(m,m);
		// vertex binding description, concise version
		OGLBuffers.Attrib[] attributes = {
				new OGLBuffers.Attrib("inPosition", 2), // 2 floats
				//new OGLBuffers.Attrib("inColor", 3) // 3 floats
		};
		buffers = new OGLBuffers(vertexBufferData, attributes,
				indexBufferData);
		// the concise version requires attributes to be in this order within
		// vertex and to be exactly all floats within vertex

/*		full version for the case that some floats of the vertex are to be ignored
 * 		(in this case it is equivalent to the concise version):
 		OGLBuffers.Attrib[] attributes = {
				new OGLBuffers.Attrib("inPosition", 2, 0), // 2 floats, at 0 floats from vertex start
				new OGLBuffers.Attrib("inColor", 3, 2) }; // 3 floats, at 2 floats from vertex start
		buffers = new OGLBuffers(gl, vertexBufferData, 5, // 5 floats altogether in a vertex
				attributes, indexBufferData);
*/
	}

	@Override
	public void init() {
		// Create the window

		OGLUtils.printOGLparameters();
		OGLUtils.printLWJLparameters();
		OGLUtils.printJAVAparameters();

		// Set the clear color
		glClearColor(0.9f, 0.9f, 0.9f, 1.0f);

		createBuffers(10);
		shaderProgramLight = ShaderUtils.loadProgram("/cv5/light.vert",
				"/cv5/light.frag",
				null,null,null,null);
		shaderProgramView = ShaderUtils.loadProgram("/cv5/view.vert",
				"/cv5/view.frag",
				null,null,null,null);


		// Shader program set
		glUseProgram(this.shaderProgramLight);

		// internal OpenGL ID of a shader uniform (constant during one draw call
		// - constant value for all processed vertices or pixels) variable
		defineUniforms();

		try {
			texture = new OGLTexture2D("textures/mapFire.jpg");
		} catch (IOException e) {
			e.printStackTrace();
		}

		cam = cam.withPosition(new Vec3D(5, 5, 2.5))
				.withAzimuth(Math.PI * 1.25)
				.withZenith(Math.PI * -0.125);
		renderTarget = new OGLRenderTarget(512, 512);
		textureViewer = new OGLTexture2D.Viewer();
		textRenderer = new OGLTextRenderer(width, height);
	}


	@Override
	public void display() {
		renderLight();

		renderView();

	}
	//definice uniform
	void defineUniforms(){
		locMatModelL = glGetUniformLocation(shaderProgramLight, "model");
		locMatViewL = glGetUniformLocation(shaderProgramLight, "view");
		locMatProjL = glGetUniformLocation(shaderProgramLight, "proj");

		locMatModelV = glGetUniformLocation(shaderProgramView, "model");
		locMatViewV = glGetUniformLocation(shaderProgramView, "view");
		locMatProjV = glGetUniformLocation(shaderProgramView, "proj");

		locType = glGetUniformLocation(shaderProgramView, "type");
		locTime = glGetUniformLocation(shaderProgramView, "time");
		locPerVertex = glGetUniformLocation(shaderProgramView, "perVertex");
		locAnim = glGetUniformLocation(shaderProgramView, "anim");
		locLight = glGetUniformLocation(shaderProgramView, "lightPos");
		locFunc = glGetUniformLocation(shaderProgramView, "func");
	}

	void renderLight(){
		renderTarget.bind();
		glClearColor(0.1f,0.9f,0.1f,1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
		glUseProgram(shaderProgramLight);
		//glViewport(0, 0, width, height);


		if (depthTest) {
			glEnable(GL_DEPTH_TEST);
		} else {
			glDisable(GL_DEPTH_TEST);
		}
		if(fill){
			glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
		} else {glPolygonMode( GL_FRONT_AND_BACK, GL_LINE);
		}
		// set the current shader to be used, could have been done only once (in
		// init) in this sample (only one shader used)

		time += 0.01;

		createUniforms();

		//texture.bind(shaderProgramLight,"textureID",0);
		// bind and draw
		buffers.draw(GL_TRIANGLES, shaderProgramLight);
		glUniformMatrix4fv(locMatModelV,false,
				model.mul(new Mat4Transl(1,1,0)).mul(new Mat4RotXYZ(xrot, yrot,zrot)).floatArray());
		buffers.draw(GL_TRIANGLES, shaderProgramLight);
	}

	void renderView(){
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glClearColor(0.6f,0.6f,0.9f,1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
		glViewport(0, 0, width, height);
		glLineWidth(2);

		glUseProgram(shaderProgramView);

		textureViewer.view(renderTarget.getColorTexture());

		createUniforms();
		buffers.draw(GL_TRIANGLES, shaderProgramView);

		renderTarget.getColorTexture().bind(shaderProgramView,"textureID",0);
	}
	//vytvoreni uniform
	void createUniforms(){
		glUniformMatrix4fv(locMatModelV,false,
				model.mul(new Mat4RotX(0)).mul(new Mat4Transl(-2,1,0)).mul(new Mat4RotXYZ(xrot, yrot,zrot)).floatArray());
		glUniformMatrix4fv(locMatViewV,false,
				cam.left(0).getViewMatrix()
						.floatArray());
		glUniformMatrix4fv(locMatProjV,false,
				proj.floatArray());
		glUniform1f(locType, types);
		glUniform1f(locTime, time);
		glUniform1i(locPerVertex, perVertex);
		glUniform1i(locAnim, anim);
		glUniform1f(locFunc, func);
		glUniform3f(locLight,(float) light_position.getX(), (float)light_position.getY(), (float) light_position.getZ());

	}


}