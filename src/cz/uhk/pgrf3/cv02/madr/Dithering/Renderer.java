package cz.uhk.pgrf3.cv02.madr.Dithering;


import lvl2advanced.p01gui.p01simple.AbstractRenderer;
import lwjglutils.*;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.*;
import transforms.*;

import java.io.IOException;
import java.nio.DoubleBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;


/**
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2019-09-02
 */
public class Renderer extends AbstractRenderer {

    double ox, oy, mouseX, mouseY;
    boolean mouseButton1 = false, mouseButton2 = false;
    // for linux
    int width = 1920;
    int height = 1080;


    OGLBuffers buffers;
    OGLTexture2D texture, bayerMatrixTex, temp;
    OGLTexture2D.Viewer textureView;
    // functions
    int ditherMode = 0;
    int orderedMode = 0;
    int th = 256;
    boolean customImage;

    int shaderProgramView,
            locOrder, locMouseX, locTh;


    Mat4 proj = new Mat4PerspRH(Math.PI / 4, 1, 0.1, 100.0);


    private GLFWKeyCallback keyCallback = new GLFWKeyCallback() {
        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
            if (action == GLFW_PRESS || action == GLFW_REPEAT) {
                switch (key) {

                    case GLFW_KEY_1:
                        th = 0;
                        orderedMode = 0;
                        if (ditherMode == 9) {
                            ditherMode = 0;
                        } else {
                            ditherMode++;
                        }
                        break;
                    case GLFW_KEY_2:
                        if (ditherMode == 2 || ditherMode == 5 || ditherMode == 6) {

                            if (orderedMode == 3) {
                                orderedMode = 0;
                            } else {
                                orderedMode++;
                            }
                        }
                        if (ditherMode == 8) {
                            if (th >= 256) {
                                th = 0;
                            } else {
                                if (th == 0) {
                                    th = 2;
                                } else {
                                    if (th < 16) {
                                        th++;
                                    } else {
                                        th = th + 16;
                                    }
                                }

                            }

                        }
                        if (ditherMode == 9) {
                            if (th == 0) {
                                th = 16;
                            }else {
                                if (th == 24) {
                                    th = 16;
                                } else {
                                    th += 8;

                                }
                            }
                        }
                        break;
                    case GLFW_KEY_LEFT_SHIFT:
                        customImage = !customImage;
                        break;

                }

            }
        }
    };

    private GLFWWindowSizeCallback wsCallback = new GLFWWindowSizeCallback() {
        @Override
        public void invoke(long window, int w, int h) {
            if (w > 0 && h > 0 &&
                    (w != width || h != height)) {
                width = w;
                height = h;
                proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.1, 100.0);
                if (textRenderer != null)
                    textRenderer.resize(width, height);
            }
        }
    };

    private GLFWMouseButtonCallback mbCallback = new GLFWMouseButtonCallback() {
        @Override
        public void invoke(long window, int button, int action, int mods) {
            mouseButton1 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;
            mouseButton2 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS;

            if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
                mouseButton1 = true;
                DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                glfwGetCursorPos(window, xBuffer, yBuffer);
                ox = xBuffer.get(0);
                oy = yBuffer.get(0);
            }

            if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE) {
                mouseButton1 = false;
                DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                glfwGetCursorPos(window, xBuffer, yBuffer);
                double x = xBuffer.get(0);
                double y = yBuffer.get(0);
                ox = x;
                oy = y;
            }
        }
    };

    private GLFWCursorPosCallback cpCallbacknew = new GLFWCursorPosCallback() {
        @Override
        public void invoke(long window, double x, double y) {
            mouseX = x;
            mouseY = y;
        }
    };

    private GLFWScrollCallback scrollCallback = new GLFWScrollCallback() {
        @Override
        public void invoke(long window, double dx, double dy) {
        }
    };

    @Override
    public GLFWKeyCallback getKeyCallback() {
        return keyCallback;
    }

    @Override
    public GLFWWindowSizeCallback getWsCallback() {
        return wsCallback;
    }

    @Override
    public GLFWMouseButtonCallback getMouseCallback() {
        return mbCallback;
    }

    @Override
    public GLFWCursorPosCallback getCursorCallback() {
        return cpCallbacknew;
    }

    @Override
    public GLFWScrollCallback getScrollCallback() {
        return scrollCallback;
    }


    void createBuffers(int m, int t) {

        float[] vertexBufferData = GridUtil.createVB();

        int[] indexBufferData = GridUtil.createIB();


        // vertex binding description, concise version
        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 2), // 2 floats
                new OGLBuffers.Attrib("inTextureCoord", 2)
        };
        buffers = new OGLBuffers(vertexBufferData, 4, attributes,
                indexBufferData);

    }

    @Override
    public void init() {
        OGLUtils.printOGLparameters();
        OGLUtils.printLWJLparameters();
        OGLUtils.printJAVAparameters();

        // Set the clear color
        glClearColor(0.8f, 0.8f, 0.8f, 1.0f);

        createBuffers(10, 0);
        shaderProgramView = ShaderUtils.loadProgram("/Dithering/view.vert",
                "/Dithering/view.frag",
                null, null, null, null);

        // Shader program set
        glUseProgram(this.shaderProgramView);

        try {
            texture = new OGLTexture2D("textures/uhk.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bayerMatrixTex = new OGLTexture2D("textures/bayerMatrixTex.png");
        } catch (IOException e) {
            e.printStackTrace();
        }


        textRenderer = new OGLTextRenderer(width, height);
        textureView = new OGLTexture2D.Viewer();


    }

    @Override
    public void display() {
        String method = "Test";
        switch (ditherMode) {
            case 0:
                method = "1.N/A";
                break;
            case 1:
                method = "2.Treshold 0.5";
                break;
            case 2:
                method = "3.Ordered";
                break;
            case 3:
                method = "4.Random";
                break;
            case 4:
                method = "5.Treshold Variable";
                break;
            case 5:
                method = "6.Ordered Color";
                break;
            case 6:
                method = "7.Random Color";
                break;
            case 7:
                method = "8.Ordered Color - BayerTexture";
                break;
            case 8:
                method = "9.Reduction Gray";
                break;
            case 9:
                method = "10. Reduction Color";
                break;


        }

        String text = new String(this.getClass().getName() + ": [1] Dithering methods, [2] Change matrix, reduction || Current: " + method);
        if (ditherMode == 2 || ditherMode == 5) {
            String orm = "Test";
            switch (orderedMode) {
                case 0:
                    orm = "thesholdMap4";
                    break;
                case 1:
                    orm = "thesholdMap9";
                    break;
                case 2:
                    orm = "thesholdMap16";
                    break;
                case 3:
                    orm = "thesholdMap64";
                    break;

            }
            text = text + " - " + orm;

        }
        if (ditherMode == 8) {
            String thes = "Divide: " + th;
            text = text + thes;
        }
        if (ditherMode == 9) {
            String colorpallete = " ,Color Pallet: " + th;
            text = text + colorpallete;
        }

        glEnable(GL_DEPTH_TEST);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


        int locMode = glGetUniformLocation(shaderProgramView, "mode");
        locOrder = glGetUniformLocation(shaderProgramView, "orderMode");
        locMouseX = glGetUniformLocation(shaderProgramView, "mouseX");
        locTh = glGetUniformLocation(shaderProgramView, "th");

        //----------------------------------------------------From View
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, width, height);
        glClearColor(0.5f, 0.1f, 0.1f, 1f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer


        glUseProgram(shaderProgramView);
        texture.bind(shaderProgramView, "textureID", 0);
        bayerMatrixTex.bind(shaderProgramView, "bayerMatrixTex", 1);

        // bind and draw
        glUniform1i(locMode, ditherMode);
        glUniform1i(locOrder, orderedMode);
        glUniform1f(locMouseX, (float) (mouseX));
        glUniform1i(locTh, th);


        //vykresleni
        buffers.draw(GL_TRIANGLE_STRIP, shaderProgramView);
        if (customImage) {
            temp = FileHelper.loadTexture();
            if (temp != null) {
                texture = temp;
            }
            customImage = false;
        }


        textRenderer.clear();
        textRenderer.addStr2D(3, 20, text);
        textRenderer.addStr2D(width - 90, height - 3, " (c) PGRF UHK Mádr Stanislav");
        textRenderer.draw();

    }

}