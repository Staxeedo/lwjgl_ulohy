package cz.uhk.pgrf3.cv02.madr.Dithering;

public class GridUtil {
    public  static float[] createVB()
    {



       float[] vertexBufferData = {
            -1, 1, 0, 0,
            1, 1, 1, 0,
            -1, -1, 0, 1,
            1, -1, 1, 1
    };

       return vertexBufferData;
    }

    public static int[] createIB()
    {
        int[] indexBufferData = {0, 1, 2, 3};
        return indexBufferData;
    }

}
