package cz.uhk.pgrf3.cv02.madr.Dithering;

import lwjglutils.OGLTexture2D;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.IOException;

public class FileHelper {
    public static OGLTexture2D loadTexture(){
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("./res/textures"));
        chooser.setFileFilter(new FileNameExtensionFilter("Texture", "jpg","png"));
        chooser.setAcceptAllFileFilterUsed(false);
        int result = chooser.showOpenDialog(null);
        if(result== JFileChooser.APPROVE_OPTION) {
            String path = chooser.getSelectedFile().getAbsolutePath().replace("\\", "/");;
            String[] name = path.split("/");
            try {

                return  new OGLTexture2D("textures/"+name[name.length-1]);

            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        else if(result == JFileChooser.CANCEL_OPTION)
        {System.out.println("No suitable File");}
        return  null;
    }
}
