package cz.uhk.pgrf3.cv02.madr.uloha1;

public class GridUtil {
    public  static float[] createVB(int m, int n)
    {
        if(m<1||n<1)
        {
            return null;
        }

        float[] vertexbuff= new float[(m+1)*(n+1)*2];
        int index = 0;
        for(int i = 0; i<=n;i++) {
            for (float j = 0; j <= m; j++) {
                vertexbuff[index++] = j*1f /m;
                vertexbuff[index++] = i*1f/n;
            }

        }

        return vertexbuff;
    }

    public static int[] createIB(int m, int n)
    {
        int[] indexbuff= new int[m*n*6];


        int index = 0;
        int k = 0;
        for(int y = 0; y<n;y++) {

            for(int x= 0;x<m;x++)
            {
                k=x+y*(m+1);
                indexbuff[index++]=k;
                indexbuff[index++]=k+(m+1);
                indexbuff[index++]=k+(m+1)+1;
                indexbuff[index++]=k;
                indexbuff[index++]=k+(m+1)+1;
                indexbuff[index++]=k+1;
            }

        }


        return indexbuff;
    }
    public static int[] createIBStrip(int m, int n)
    {
        int[] indexbuffstrip = new int[((m+1)*(n+1)*2)-2];
        int index = 0;
        int k;

        for (int j = 0; j < n ; j++)
        {
            k=j*(m+1);
            for (int i = 0; i < m+1 ; i++) {
                if (j % 2 == 0) {
                    if (i == m) {
                        indexbuffstrip[index++] = k + i;
                        indexbuffstrip[index++] = k + (n + 1) + i;
                        indexbuffstrip[index++] = k + (n + 1) + i;
                        indexbuffstrip[index++] = k + (n + 1) + i;
                    } else {
                        indexbuffstrip[index++] = k + i;
                        indexbuffstrip[index++] = k + (n + 1) + i;
                    }
                } else {
                    if (i == m) {
                        indexbuffstrip[index++] = k + (n + 1);
                        indexbuffstrip[index++] = k;
                        indexbuffstrip[index++] = k;
                        indexbuffstrip[index++] = k;
                    } else {
                        indexbuffstrip[index++] = k + 2 * (m + 1) - 1 - i;
                        indexbuffstrip[index++] = k + m - i;
                    }
                }

            }

        }
//        for(int i : indexbuffstrip) {
//            System.out.print(i + " ");
//        }
        return indexbuffstrip;
    }
}
