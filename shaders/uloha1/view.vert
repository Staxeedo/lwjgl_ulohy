#version 150
#define PI 3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117067982148086513282306647
in vec2 inPosition;// input from the vertex buffer
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform int mode;
uniform int colormode;
uniform int lighttype;
uniform vec3 viewPos;
uniform vec3 lightPos;
uniform float time;
uniform vec3 spotDir;
out vec2 posIO;
out vec4 objPos;
out vec3 normal_IO;
out vec3 lightDir;
out vec3 color;
out vec3 viewDir;
out float lightDist;
out vec3 spotDir_IO;
out float intensity;



float getFValue(vec2 xy){
    if (mode==0)
    return 0.;
    if (mode==2)
    {
        float s = xy.y * 2.0*PI;
        float z = sin(s);
        return z;
    }

    if (mode==3)
    {
        float t = xy.x* 2.0*PI;
        float z = sin(t)/3;
        return z;
    }
    if (mode==4)
    {
        float t = PI*xy.x;
        float r = 1;
        float phi = t;
        float z = r* cos(phi);

    }

    if (mode==5)
    {
        float t = PI*xy.x;
        float s = 2*PI*xy.y;
        float phi = t;
        float r = 3+cos(4*s);
        float z = r* cos(phi);

    }

    if (mode==6)
    {

        float t = 2*PI * xy.y;

        float z = 2*sin(t);
        return z;
    }

    if (mode==7){
        float t =2* PI* xy.y;
        float z = 2 - t;
        return z;
    }

    return -(xy.x*xy.x*5+xy.y*xy.y*5);
}

vec3 functionExample(vec2 xy)
{
    float z = getFValue(xy);
    return vec3(xy.x, xy.y, z);

}
vec3 functionCM(vec2 xy)
{
    //My
    float t = xy.x* 2.0*PI;
    float s = xy.y * 2.0*PI;

    float x = cos(t);
    float y = sin(t)+cos(s);
    float z = sin(s);
    return vec3(x, y, z);
}
vec3 functionC(vec2 xy){
    //Torus
    float t = xy.x* 2.0*PI;
    float s = xy.y * 2.0*PI;
    float x = (3*cos(s)+cos(t)*cos(s))/3;
    float y = (3*sin(s)+cos(t)*sin(s))/3;
    float z = sin(t)/3;
    return vec3(x, y, z);
}
vec3 functionS(vec2 xy)
{
    float t = PI*xy.x;
    float s = 2*PI*xy.y;
    float r = 1;
    float phi = t;
    float theta = s;

    float x = r * sin(phi) * cos(theta);
    float y = r * sin(phi) * sin(theta);
    float z = r* cos(phi);
    return vec3(x, y, z);
}

vec3 functionS2(vec2 xy)
{
    float t = PI*xy.x;
    float s = 2*PI*xy.y;
    float r = 3+cos(4*s);
    float phi = t;
    float theta = s;

    float x = r * sin(phi) * cos(theta);
    float y = r * sin(phi) * sin(theta);
    float z = r* cos(phi);
    return vec3(x, y, z);
}
vec3 functionCyl(vec2 xy)
{
    float s = 2*PI * xy.x;
    float t = 2*PI * xy.y;
    float theta = s;
    float r = t;


    float x = r * cos(theta);
    float y = r * sin(theta);
    float z = 2*sin(t);
    return vec3(x, y, z);


}
vec3 functionCyl2(vec2 xy)
{
    float s = 2*PI * xy.x;
    float t = 2*PI* xy.y;

    float theta = s;
    float r = 1 + cos(t);


    float x = r * cos(theta);
    float y = r * sin(theta);
    float z = 2 - t;
    return vec3(x, y, z);

}

vec3 getFinalPosition(vec2 position)
{
    vec3 finalPosition;
    switch (mode){
        case 0:
        finalPosition= vec3(position.x, position.y, 0);
        break;
        case 1:
        finalPosition= functionExample(position);
        break;
        case 2:
        finalPosition= functionCM(position);
        break;
        case 3:
        finalPosition=functionC(position);
        break;
        case 4:
        finalPosition=functionS(position);
        break;
        case 5:
        finalPosition=functionS2(position);
        break;
        case 6:
        finalPosition=functionCyl(position);
        break;
        case 7:
        finalPosition=functionCyl2(position);
        break;
    }
    return finalPosition;
}

vec3 getNormal(vec2 xy){
    float delta = 0.01;
    vec3 u = getFinalPosition(xy + vec2(delta, 0)) - getFinalPosition(xy - vec2(delta, 0));


    vec3 v = getFinalPosition(xy + vec2(0, delta)) - getFinalPosition(xy - vec2(0, delta));

    return cross(u, v);
}

void main() {
    // pozice v tom gridu
    posIO = inPosition;

    vec2 position;
    if (mode >=4){
        position = inPosition;
    }
    else {
        position = inPosition -  0.5;
    }

    vec3 finalPosition = getFinalPosition(position);


    vec3 normal =transpose(inverse(mat3(view * model))) * normalize(getNormal(position.xy));

    //PerVertex
    if (lighttype == 0) {
        normal = mat3(view) * getNormal(position.xy);
        normal = normalize(normal);
        normal = inverse(transpose(mat3(view))) * normal;
    }
    normal_IO = normal;

    if (colormode==0)
    {
        color= (model * vec4(finalPosition, 1.0)).xyz;
    }
    else {
        color= vec3(normal);
    }


    objPos = model*vec4(finalPosition.x, finalPosition.y, finalPosition.z, 1.0);

    //animace
    if(mode==0)
    {
        objPos.x=objPos.x*time;
        objPos.y=objPos.y*time;
    }

    //reflektor
    spotDir_IO = normalize(lightPos - vec3(model * vec4(finalPosition, 1.0)));

    lightDir=normalize(lightPos.xyz-(model*objPos).xyz);
    viewDir=normalize(((mat3(view) * viewPos))- vec3(view*objPos));
    lightDist = length(viewDir);
    intensity = dot(normalize(lightDir), normalize(normal));


    gl_Position = proj*view*objPos;
} 
