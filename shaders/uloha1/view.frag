#version 150
out vec4 outColor;// output from the fragment shader
uniform sampler2D textureID;
uniform sampler2D textureDepth;
uniform mat4 matMVPlight;
uniform int mode;
uniform int colormode;
uniform int lightmode;
uniform int lighttype;
in vec2 posIO;
in vec4 objPos;
in vec3 normal_IO;
in vec3 lightDir;
in vec3 viewDir;
in vec3 color;
in float lightDist;
in vec3 spotDir_IO;
in float intensity;

void main() {


    //B-F
    vec4 ambient = vec4(vec3(0.15),1);

    float NdotL = max(0, dot(normalize(normal_IO), normalize(lightDir)));
    vec4 diffuse = vec4(NdotL*vec3(0.4), 1);


    vec3 halfD = normalize(lightDir + viewDir);
    float NdotH = dot(normalize(normal_IO), halfD);
    vec4 specular = vec4(pow(NdotH, 16) * vec3(1), 1);


    float att = 1.0/(1.0+0.14*lightDist+0.07*(lightDist*lightDist));



    //shadow
    vec4 shadowCoord = matMVPlight* vec4(objPos.xyz, 1.0);

    vec3 texCoord = shadowCoord.xyz/shadowCoord.w*0.5+0.5;
    texCoord.z = texCoord.z - 0.005;



    if (texture(textureDepth, texCoord.xy).z < texCoord.z){
        outColor = vec4(1.0);
    } else {

        if(colormode==0)
        {
            if(lightmode==0)
            outColor = vec4(color,1.0)*(ambient+diffuse+specular);
            if(lightmode==1)
            outColor = vec4(color,1.0)*ambient + att * (diffuse + specular);
            if(lightmode==2)
            {
                float spotCutOff = 0.7;
                vec3 ld=normalize(lightDir);
                float spotEffect = max(dot(normalize(spotDir_IO),normalize(-ld)),0);
                if (spotEffect > spotCutOff) {
                    outColor=(ambient*vec4(color,1.0)) + att*((diffuse*NdotL*vec4(color,1.0)) + (specular*((pow(NdotH,0.5*4.0)))));

                }else
                {
                    outColor=ambient*vec4(color,1.0);
                }

            }
        }
        if (colormode==1)
        {
            if(lightmode==0)
            outColor = vec4(texture(textureID, posIO).rgb, 1.0)*ambient+att*(diffuse+specular);
            if(lightmode==1)
            outColor = vec4(texture(textureID, posIO).rgb, 1.0)*ambient + att * (diffuse + specular);
            if(lightmode==2)
            {
                float spotCutOff = 0.7;
                vec3 ld=normalize(lightDir);
                float spotEffect = max(dot(normalize(spotDir_IO),normalize(-ld)),0);
                if (spotEffect > spotCutOff) {
                    outColor=(ambient*vec4(texture(textureID, posIO).rgb, 1.0)) + att*((diffuse*NdotL*vec4(texture(textureID, posIO).rgb, 1.0)) + (specular*((pow(NdotH,0.5*4.0)))));

                }else
                {
                    outColor=ambient*vec4(texture(textureID, posIO).rgb, 1.0);
                }

            }
        }



        float cosAlpha;
        if(colormode==2)
        {   //normal
            cosAlpha=max(dot(normalize(normal_IO),normalize(lightDir)),0.0);
            if(lightmode==0)
            outColor = vec4(vec3(cosAlpha),1.0)*(ambient+diffuse+specular);
            if(lightmode==1)
            outColor = vec4(vec3(cosAlpha),1.0)*ambient + att * (diffuse + specular);

        }

        if(lighttype==0)
        {
            if (intensity > 0.95){
                outColor = vec4(1.0, 0.5, 0.5, 1.0);
            }
            else if (intensity > 0.80){
                outColor = vec4(0.6, 0.3, 0.3, 1.0);
            }
            else if (intensity > 0.50){
                outColor = vec4(0.0, 0.0, 0.3, 1.0);
            }
            else if (intensity > 0.25){
                outColor = vec4(0.4, 0.2, 0.2, 1.0);
            }
            else{
                outColor= vec4(0.2, 0.1, 0.1, 1.0);
            }

        }
        if (lighttype==1) {
            float intensityPP= dot(normalize(lightDir), normalize(normal_IO));
            if (intensityPP > 0.95){
                outColor = vec4(1.0, 0.5, 0.5, 1.0);
            }
            else if (intensityPP > 0.80){
                outColor = vec4(0.6, 0.3, 0.3, 1.0);
            }
            else if (intensityPP > 0.50){
                outColor = vec4(0.0, 0.0, 0.3, 1.0);
            }
            else if (intensityPP > 0.25){
                outColor = vec4(0.4, 0.2, 0.2, 1.0);
            }
            else{
                outColor= vec4(0.2, 0.1, 0.1, 1.0);
            }
        }

        if(mode==4)
        {
            //slunce
            outColor = vec4(1, 1, 0, 0);
        }
    }
}
