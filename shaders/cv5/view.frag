#version 150
in vec3 color;
in vec3 normal_IO;
in vec3 lightDir;
in vec2 posIO;
in float intensity;
in float distance;
out vec3 viewDirection;
out vec4 outColor; // output from the fragment shader
uniform sampler2D textureID;
uniform float type;
uniform int perVertex;
uniform float amb;
uniform float diff;
uniform float spec;

void main() {
    float cosAlpha = max(0,dot(normalize(normal_IO), normalize(lightDir)));
    if(type == 0){
        //textura
        outColor = vec4(texture(textureID,posIO).rgb,1.0);
    } else if (type == 1 || type == 2){
        //osvetleni
        vec4 ambient = vec4(0.0);
        vec4 diffuse = vec4(0.0);
        vec4 specular = vec4(0.0);

        vec4 finalColor = vec4(vec3(posIO,1.0),1);
        ambient = amb * finalColor;
        //skalarni soucin normala a vektor ke svetlu
        float NdotL=max(dot(normalize(normal_IO), normalize(lightDir)),0);
        //difuzni slozka
        diffuse = NdotL * diff * finalColor;

        //specularni slozka
        vec3 halfVector = normalize(lightDir) + normalize(viewDirection);
        float NdotH =  max( 0.0, dot(normalize(normal_IO), halfVector));
        specular = spec * vec4(pow(NdotH,1));

        //utlum svetla
        float att = 1.0/(1.0 + 0.1 * distance + 0.1 * distance * distance);
        //float spotCutOff = 0;
        //vec3 spotDirection = vec3(1);
        //float spotEffect =  max(dot(normalize(spotDirection),normalize(-lightDir)),0);
        //if(spotCutOff < spotEffect){
        //} else {
           // outColor = ambient;
        //}

        //gl_FragColor = vec4(texture(textureID,posIO).rgb,1.0) *vec4(vec3(color), 1.0);
       if (type == 1){
           outColor = (ambient + att*(diffuse + specular));
       } else {
           outColor = 0.4 *vec4(texture(textureID,posIO).rgb,1.0) + 0.6*(ambient + att*(diffuse + specular));
       }
    } else if (type == 3){
        //barevny
        outColor = vec4(color,1.0);
    } else if (type == 4){
        //jedna barva
        outColor = vec4(0, 1, 0, 1.0);
    } else {
        //obarveni skokove
        float intensityFinal;
        if(perVertex == 0){
            //per pixel
            intensityFinal = dot(normalize(lightDir), normalize(normal_IO));
        } else {
            //per vertex
            intensityFinal = intensity;
        }

        if (intensityFinal>0.95) outColor=vec4(1.0, 0.5, 0.5, 1.0);
        else if (intensityFinal>0.8) outColor=vec4(0.6, 0.3, 0.3, 1.0);
        else if (intensityFinal>0.5) outColor=vec4(0.0, 0.0, 0.3, 1.0);
        else if (intensityFinal>0.25) outColor=vec4(0.4, 0.2, 0.2, 1.0);
        else outColor=vec4(0.2, 0.1, 0.1, 1.0);
    }

} 
