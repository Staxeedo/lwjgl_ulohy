#version 150
in vec2 inPosition; // input from the vertex buffer
uniform mat4 MVP;
out vec3 color;
out vec3 normal_IO;

float getFvalue(vec2 xy){
    return - (xy.x*xy.x*5+xy.y*xy.y*5);
}


vec3 getNormal(vec2 xy){
    //osvetleni
    // o kolik odskocime
    float epsylon=0.01;
    vec3 u = vec3(xy.x + epsylon, xy.y, getFvalue(xy + vec2(epsylon,0)))
    -vec3(xy - vec2(epsylon,0), getFvalue(xy-vec2(epsylon,0)));
    vec3 v = vec3(xy+vec2(0,epsylon), getFvalue(xy+vec2(0,epsylon)))
    -vec3(xy-vec2(0,epsylon),getFvalue(xy-vec2(0,epsylon)));
    return cross(u,v);
}

void main() {
	vec2 position = inPosition;
	position.xy += -0.5;
	float z = getFvalue(position.xy);
	vec3 normal = normalize (getNormal(position.xy));
	normal_IO = normal;
	color= vec3(normal);
	gl_Position = MVP*vec4(position.x, z, position.y,1.0);
} 
