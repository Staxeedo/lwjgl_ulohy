#version 150
in vec3 color;
in vec3 normal_IO;
out vec4 outColor; // output from the fragment shader

void main() {
    vec3 lightPosition = vec3(10);
    float cosAlpha = dot(normal_IO,normalize(lightPosition));
	//outColor = vec4(0.5, 0.5, 0.5, 0.5);
	//outColor = vec4(color,1.0);
	outColor = vec4(vec3(cosAlpha),1.0);
} 
