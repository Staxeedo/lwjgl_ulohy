#version 150
in vec3 color;
in vec3 normal_IO;
out vec4 outColor; // output from the fragment shader
in vec3 lightDir;
in vec2 positionIO;
uniform sampler2D textureID;
void main() {


   float cosAlpha = max(dot(normalize(normal_IO),normalize(lightDir)));
	//outColor = vec4(0.5, 0.5, 0.5, 0.5);
	//outColor = vec4(color,1.0);
   //outColor = vec4(vec3(cosAlpha),1.0);



   //textura
   outColor = vec4(texture(textureID,positionIO).rgb,1.0);
} 
