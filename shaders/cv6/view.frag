#version 150
out vec4 outColor; // output from the fragment shader
uniform sampler2D textureID;
uniform sampler2D textureDepth;
uniform mat4 matMVPlight;
in vec2 posIO;
in vec4 objPos;

void main() {

    vec4 shadowCoord = matMVPlight* vec4(objPos.xyz, 1.0);
    vec3 texCoord = shadowCoord.xyz/shadowCoord.w*0.5+0.5;

    if (texture(textureDepth, texCoord.xy).z < texCoord.z){
        outColor = vec4(1.0);
    } else{
        outColor = vec4(texture(textureID,posIO).rgb, 1.0);
    }
}
