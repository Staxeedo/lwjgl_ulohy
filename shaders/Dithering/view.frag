#version 150
out vec4 outColor;// output from the fragment shader
uniform sampler2D textureID;
uniform sampler2D bayerMatrixTex;
uniform int mode;
uniform int orderMode;
uniform float mouseX;
uniform int th;
in vec2 textureCoord;


struct Cgc
{
    float color;
    float distance;
};
struct Ccc
{
    vec3 color;
    float distance;
};

const vec3 colorPallet16[16]=vec3[] (vec3(255.0, 255.0, 255.0), vec3(170.0, 170.0, 170.0), vec3(85.0, 85.0, 85.0), vec3(0, 0, 0),
vec3(255.0, 255.0, 85.0), vec3(0, 170.0, 0), vec3(85.0, 255.0, 85.0), vec3(255.0, 85.0, 85.0), vec3(170.0, 0, 0), vec3(170.0, 85.0, 0),
vec3(170.0, 0, 170.0), vec3(255.0, 85.0, 255.0), vec3(85.0, 255.0, 255.0), vec3(0, 170.0, 170.0), vec3(0, 0, 170.0),
vec3(85.0, 85.0, 255.0));
const vec3 colorPallet23[23]=vec3[](
vec3(255, 255, 0), vec3(255, 102, 0), vec3(128, 128, 0), vec3(0, 255, 0), vec3(0, 128, 0), vec3(66, 154, 167), vec3(0, 128, 128),
vec3(0, 55, 60), vec3(0, 255, 255), vec3(0, 0, 255), vec3(0, 0, 128), vec3(153, 153, 255), vec3(102, 102, 204), vec3(51, 51, 102),
vec3(255, 0, 0), vec3(128, 0, 0), vec3(255, 102, 204), vec3(153, 0, 102), vec3(255, 255, 255), vec3(192, 192, 192), vec3(128, 128, 128),
vec3(34, 34, 34), vec3(0, 0, 0));


const int thesholdMap4[4] = int[]
(0, 2,
3, 1);

const int thesholdMap9[9] = int[]
(0, 7, 3,
6, 5, 2,
4, 1, 8);

const int thesholdMap16[16] = int[]
(0, 8, 2, 10,
12, 4, 14, 6,
3, 11, 1, 9,
15, 7, 13, 5);

const int thesholdMap64[64] = int[]
(0, 48, 12, 60, 3, 51, 15, 63,
32, 16, 44, 28, 35, 19, 47, 31,
8, 56, 4, 52, 11, 59, 7, 55,
40, 24, 36, 20, 43, 27, 39, 23,
2, 50, 24, 62, 1, 49, 13, 61,
34, 18, 46, 30, 33, 17, 45, 29,
10, 58, 6, 54, 9, 57, 5, 53,
42, 26, 38, 22, 41, 25, 37, 21);
vec2 modulo(int num){
    return vec2(int(mod(gl_FragCoord.x, num)),
    int(mod(gl_FragCoord.y, num)));
}
float getValue(int orderMode){
    vec2 val;
    switch (orderMode%4){
        case 0:
        val = modulo(2);
        return thesholdMap4[int(val.x) + int(val.y)*2] * (1 / 4.0);
        case 1:
        val = modulo(3);
        return thesholdMap9[int(val.x) + int(val.y)*3] * (1/9.0);
        case 2:
        val = modulo(4);
        return thesholdMap16[int(val.x) + int(val.y)*4] *(1/16.0);
        default :
        val = modulo(8);
        return thesholdMap64[int(val.x) + int(val.y)*int(8)] * (1/64.0);
    }

}
float tresholdDithering(float color, float treshold) {

    if (color < treshold){
        return 0.0;
    }
    return 1.0;
}
float orderedDithering(float color, int orderMode){
    float d = getValue(orderMode);

    if (color < d)
    return 0.0;
    return 1.0;

}
vec3 orderedDitheringTex()
{
    vec2 val = modulo(8);
    return texture2D(bayerMatrixTex, (val / 8)).rgb;
}

float random(vec2 p)
{
    //https://stackoverflow.com/questions/5149544/can-i-generate-a-random-number-inside-a-pixel-shader
    // e^pi (Gelfond's constant)
    // 2^sqrt(2) (Gelfond–Schneider constant)
    vec2 K1 = vec2(23.14069263277926, 2.665144142690225);

    return fract(cos(dot(p, K1)) * 12345.6789);
}
float randomDithering(float color) {
    if (color < random(vec2(gl_FragCoord.xy)))
    return 0.0;
    return 1.0;
}
float reduceGrayColor(float color, int th) {
    float theshold = 1.0/th;
    float center, gainedColor, distance;
    Cgc closestGrayColor;



    if (color<theshold)
    return 0;
    if (color> (1.0-theshold))
    {
        return 1;
    }
    //find closest color
    int step=0;
    for (float i = theshold;i<(1.0-theshold);i=i+theshold)
    {
        center = (i+theshold)/ 2;
        gainedColor=i+center;

        distance=abs(gainedColor-color);

        if (step==0)
        {
            closestGrayColor.distance=distance;
            closestGrayColor.color=gainedColor;
        } else
        {
            if (distance<closestGrayColor.distance)
            {
                closestGrayColor.color=gainedColor;
                closestGrayColor.distance=distance;
            }
        }
        step++;
    }

    return closestGrayColor.color;

}
//Calculate Color distance
float colorDistance(vec3 c1, vec3 c2)
{
    c1.r = c1.r/255.0;
    c1.g = c1.g/255.0;
    c1.b = c1.b/255.0;
    float r = abs(c1.r-c2.r);
    float g = abs(c1.g - c2.g);
    float b = abs(c1.b - c2.b);
    return sqrt((r*r)*((0.3)*(0.3))+(g*g)*(0.59*0.59)*+(b*b)*(0.11*0.11));
}

vec3 reduceColorByPallete(vec3 color, int th) {
    Ccc currentClosestColor;
    float distance;
    for (int i = 0; i<th;i++){
        if (th==16){
            distance = colorDistance(colorPallet16[i], color);
        } else {
            distance = colorDistance(colorPallet23[i], color);
        }

        if (i==0)
        {
            if (th==16){
                currentClosestColor.color=colorPallet16[i];
            } else
            {
                currentClosestColor.color=colorPallet23[i];
            }
            currentClosestColor.distance = distance;
        } else {
            if (currentClosestColor.distance > distance)
            {
                if (th==16){
                    currentClosestColor.color=colorPallet16[i];
                } else
                {
                    currentClosestColor.color=colorPallet23[i];
                }
                currentClosestColor.distance = distance;
            }
        }

    }
    return currentClosestColor.color;
}

void main() {
    float tempColor;
    vec3 tColor;
    tempColor = dot(texture2D(textureID, textureCoord).xyz, vec3(1/3.));
    tColor = texture2D(textureID, textureCoord).rgb;

    switch (mode){
        case 0:
        if (gl_FragCoord.x<mouseX)
        outColor = vec4(tColor, 1.0);
        else
        outColor = vec4(tempColor, tempColor, tempColor, 1.0);
        break;
        case 1:
        if (gl_FragCoord.x<mouseX)
        outColor = vec4(tempColor, tempColor, tempColor, 1.0);
        else
        outColor = vec4(vec3(tresholdDithering(tempColor, 0.5)), 1.0);
        break;
        case 2:
        if (gl_FragCoord.x<mouseX)
        outColor = vec4(tempColor, tempColor, tempColor, 1.0);
        else
        outColor = vec4(vec3(orderedDithering(tempColor, orderMode)), 1.0);
        break;
        case 3:
        if (gl_FragCoord.x<mouseX)
        outColor = vec4(tempColor, tempColor, tempColor, 1.0);
        else
        outColor = vec4(vec3(randomDithering(tempColor)), 1.0);
        break;
        case 4:
        outColor = vec4(vec3(tresholdDithering(tempColor, float((int(mouseX) % 600)/600.))), 1.0);
        break;
        case 5:
        if (gl_FragCoord.x<mouseX)
        outColor = vec4(tColor, 1.0);
        else {
            outColor = vec4(vec3(orderedDithering(tColor.r, orderMode),
            orderedDithering(tColor.g, orderMode),
            orderedDithering(tColor.b, orderMode)
            ), 1);
        }
        break;
        case 6:
        if (gl_FragCoord.x<mouseX)
        outColor = vec4(tColor, 1.0);
        else
        outColor = vec4(vec3(
        randomDithering(tColor.r),
        randomDithering(tColor.g),
        randomDithering(tColor.b)
        ), 1.0);
        break;
        case 7:
        if (gl_FragCoord.x<mouseX)
        outColor = vec4(tColor, 1.0);
        else {
            outColor = vec4(floor(tColor + (tColor*vec3(orderedDitheringTex()))), 1);
        }
        break;
        case 8:
        if (gl_FragCoord.x<mouseX)
        outColor = vec4(tempColor, tempColor, tempColor, 1.0);
        else
        outColor=vec4(vec3(reduceGrayColor(tempColor, th)), 1.0);
        break;
        case 9:
        if (gl_FragCoord.x<mouseX)
        outColor = vec4(tColor, 1.0);
        else
        outColor=vec4(reduceColorByPallete(tColor, th), 1.0);
        break;


    }
}

