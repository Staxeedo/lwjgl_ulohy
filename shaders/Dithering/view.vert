#version 150
in vec2 inPosition;// input from the vertex buffer
in vec2 inTextureCoord;
out vec2 textureCoord;

void main() {
    gl_Position = vec4(inPosition.x,inPosition.y - 0.05,0.0, 1.1);
    textureCoord = inTextureCoord;
} 
